/**
 * Global variable for the SVG drawing
 **/
var svgDoc;

/**
 * the global variable for a Multimeter instance used in the demonstration
 **/
var mm;

/**
 * Global variable containing all the nodes
 **/
var Nodes=[];
var linkingNode=null; // the only node which is building a link at a time

/**
 * A board for jsxgraph plots
 **/
var board;

/**
 * audio stuff for the buzzer
 **/
var audioCtx = new(window.AudioContext || window.webkitAudioContext)();
var oscillator = null,  buzzing = false;

// Macro function plotter
function addCurve(board, func, atts) {
    var f = board.create('functiongraph', [func], atts);
    return f;
}
// Simplified plotting of function
function plot(func, atts) {
   if (atts==null) {
      return addCurve(board, func, {strokewidth:2});
   } else {
      return addCurve(board, func, atts);
   }    
}
function clearAll(board, imin, imax, vmin, vmax) {
    if (typeof(imin) == "undefined") imin = -1;
    if (typeof(imax) == "undefined") imax = 1;
    if (typeof(vmin) == "undefined") vmin = -10;
    if (typeof(vmax) == "undefined") vmax = 10;
    JXG.JSXGraph.freeBoard(board);
    board = JXG.JSXGraph.initBoard('box', {boundingbox:[imin,vmax,imax,vmin], axis:true});
    return board;
}


/**
 * decimal logarithm
 **/
function log10(x){
    return Math.log(x)/Math.log(10);
}

/**
 * number of digits of a number before its decimal dot
 **/
function beforeDot(x){
    return Math.floor(log10(x))+1
}

/**
 * @class Node
 * to implement electrical nodes. Each node has a potential, and
 * the sum of currents arriving in a node is zero (in permanent regime)
 * 
 * Nodes can be linked to each other, and they have a property "linking"
 * which is true when someone is creating a link originating from this node.
 **/
class Node {
    /**
     * Parameters of the constructor:
     * @param id an identifier
     * @param el a jQuery element in the SVG drawing
     * @param parent a link to a parent objet; the couple (parent, id) should
     *   be unique.
     **/
    constructor(id, el, parent){
	this.id = id;
	this.el = el;
	this.el.css({cursor: "pointer"});
	this.parent = parent;
	this.links = []; // list of nodes linked by a wire
	this.line = null;
	this.wires = []; // list of wires originated here 
    }
}

/**
 * @class UI
 * a class to keep a voltage (u) and a current (i)
 **/
class UI {
    constructor (u,i){
	this.voltage=u;
	this.current=i;
    }

    get u(){
	return this.voltage;
    }

    get i(){
	return this.current;
    }
}

/**
 * solve electrical problems where two dipoles are connected, each one
 * described by its characteristic ui function, i.e. the function which
 * yelds the voltage u when is given the current i.
 *
 * param uifunc1 a function which yields u, given i
 * param uifunc1 another function which yields u, given i
 * @retun the solution when u and i are the same for both functions
 */
function solve( uifunc1, uifunc2){
    var uifunc = function(i){return uifunc1(i) - uifunc2(i)}
    var imax = 11, imin = -10;
    var epsilon = 1e-14;
    if ((uifunc(imax) * uifunc(imin)) > 0){
	// This is an error condition : voltages are the same sign
	// for both imin and imax
	// throw "Cannot solve the equations."
	board = clearAll(board);
	c1=plot(function(x){return uifunc1(x)});
	c2=plot(function(x){return uifunc2(x)},{strokecolor:'red', dash:1});
	return new UI(1000, 10);
    }
    var i;
    while ((imax - imin) > epsilon){
	i = (imax+imin)/2;
	if ((uifunc(imax) * uifunc(i)) >= 0) {
	    // the voltages have the same signs for imax and i; change imax
	    imax = i;
	} else {
	    // the voltages have opposite signs for imax and i; change imin
	    imin = i;
	}
    }
    var ui = new UI(uifunc1(i), i);
    var imin = -1e-4, imax = 1e-4, vmin = -1e-3, vmax = 1e-3;
    if (Math.abs(ui.i) > imax){
	imin = -1.5 * Math.abs(ui.i);
	imax = -imin;
    }
    if (Math.abs(ui.u) > vmax){
	var vmin = -1.5 * Math.abs(ui.u);
	var vmax = -vmin;
    }
    board = clearAll(board, imin, imax, vmin, vmax);
    c1=plot(function(x){return uifunc1(x)});
    c2=plot(function(x){return uifunc2(x)},{strokecolor:'red', dash:1});
    return ui;
}

/**
 * @class Feature
 * description of a multimeter's feature when some function is selected.
 *
 * The multimeter is considered as a resistor (finite or not) which
 * can be connected to an external electric setup by two points.
 * Eventually, it can have an internal source of voltage, for example
 * when used to test external resistors, diode junctions or electric.
 * continuity.
 **/
class Feature {

    /**
     * @param setting a jQuery element from the SVG tree, with
     *   attribute data-category == "setting"
     * @param parent the parent multimeter
     **/
    constructor(setting, parent){
	if (buzzing){
	    oscillator.stop(); // stop any buzzer
	    buzzing= false;
	}
	/* default */
	this.r = 5e7;
	this.E = 0;
	this.parent = parent;
	this.max = 500;
	this.f = function(multimeter){
	    return function(){
		multimeter.setDisplay("");
	    }
	}(this.parent);
	var unit = null, diode = false, buzzer = false;
	if(setting) {
	    unit = setting.attr("data-unit");
	    diode = setting.attr("id") == "diode";
	    buzzer = setting.attr("id") == "buzzer";
	}
	/*************** BUZZER *****************************/
	if (buzzer){
	    this.rdm = { r: 1e9,   d:3, m: 1e3, x: 200 };  /*200   ohm */
	    this.max = this.rdm.x
	    this.E = 1e6; // fake big value to make easily a current source
	    this.f = function (obj){
		return function(uifunc){
		    var ui = solve(uifunc, function(i){return obj.E + i * obj.rdm.r});
		    obj.parent.setDisplay(obj.rdm.m * ui.u, obj.max, obj.rdm.d);
		    if (Math.abs(ui.u) < 0.2) {
			// low resistance, buzz!
			if (!buzzing){
			    oscillator = audioCtx.createOscillator();
			    oscillator.type = 'square';
			    oscillator.frequency.value = 880;
			    oscillator.connect(audioCtx.destination);
			    oscillator.start();
			    buzzing = true;
			}
		    } else {
			// high resistance, don't buzz
			if (buzzing){
			    oscillator.stop(); // stop any buzzer
			    buzzing= false;
			}
		    }
		}
	    }(this);
	}
	/*************** DIODE *****************************/
	if (diode){
	    this.E = 9;      // emf of the battery
	    this.r = 1e5     // 100 kOhm, current near 100 µA
	    this.max = 2000  // range = 2000 mV
	    this.dotpos = 4
	    this.f = function (obj){
		return function(uifunc){
		    var ui = solve(uifunc, function(i){return obj.E + i * obj.r});
		    obj.parent.setDisplay(1000*ui.u, obj.max, obj.dotpos);
		}
	    }(this);
	    
	}
	/*************** VOLTMETER *****************************/
	if (! diode && unit == "V"){
	    var rdm={
		/* r resistance, d dot position, m multiplicator, x max */
		500: { r: 1e7,  d:4, m: 1e0, x: 500 },  /*500 V */
		200: { r: 1e7,  d:3, m: 1e0, x: 200}, /*200 V */
		20:  { r: 1e7,  d:2, m: 1e0, x: 20  }, /*20 V */
		2:   { r: 1e7,  d:4, m: 1e3, x: 2000 }, /*2000 mV */
		0.2: { r: 1e7,  d:3, m: 1e3, x: 200}, /*200 mV */
	    };
	    var entry = parseFloat(setting.attr("data-value"));
	    this.rdm = rdm[entry];
	    this.max = this.rdm.x
	    if (setting.attr("data-acdc") == "dc") {
		//direct current
		this.f = function (obj){
		    return function(uifunc){
			var ui = solve(uifunc, function(i){return i * obj.rdm.r});
			obj.parent.setDisplay(obj.rdm.m * ui.u, obj.max, obj.rdm.d);
		    }
		}(this);
	    } else {
		//alternative current so the external function uifunc
		// is distorted
		this.f = function (obj){
		    return function(uifunc){
			var uifunc2 = function(i){
			    var urectified = Math.abs(uifunc(i));
			    if (urectified > 0.7) urectified -= 0.7;
			    else urectified = 0;
			    return urectified*Math.sqrt(2);
			}
			var ui = solve(uifunc2, function(i){return obj.E + i * obj.r});
			obj.parent.setDisplay(ui.u, obj.max, obj.dotpos);
		    }
		}(this);
	    }
	}
	/*************** OHMMETER *****************************/
	if (unit == "ohm"){
	    var rdm={
		/* r resistance, d dot position, m multiplicator, x max */
		200:     { r: 1e9,   d:3, m: 1e3, x: 200 },  /*200   ohm */
		2000:    { r: 1e10,  d:4, m: 1e4, x: 2000}, /*2000  ohm */
		20000:   { r: 1e11,  d:2, m: 1e2, x: 20  }, /*20k   ohm */
		200000:  { r: 1e12,  d:3, m: 1e3, x: 200 }, /*200k  ohm */
		2000000: { r: 1e13,  d:4, m: 1e4, x: 2000}, /*2000k ohm */
	    };
	    var entry = parseFloat(setting.attr("data-value"));
	    this.rdm = rdm[entry];
	    this.max = this.rdm.x
	    this.E = 1e6; // fake big value to make easily a current source
	    this.f = function (obj){
		return function(uifunc){
		    var ui = solve(uifunc, function(i){return obj.E + i * obj.rdm.r});
		    obj.parent.setDisplay(obj.rdm.m * ui.u, obj.max, obj.rdm.d);
		}
	    }(this);
	}
	/*************** AMMETER *****************************/
	if (unit == "A"){
	    var rdm={
		/* r shunt resistance, d dot position, m multiplicator, x max */
		10:   { r: 1e-3, d:2, m: 1e4, x: 10  }, /*10 A */
		2e-1: { r: 1e-1, d:3, m: 1e4, x: 200 }, /*200  mA */
		2e-2: { r: 1e0 , d:2, m: 1e3, x: 20  }, /*20   mA */
		2e-3: { r: 1e1 , d:4, m: 1e5, x: 2000}, /*2000  µA */
		2e-4: { r: 1e2 , d:3, m: 1e4, x: 200 }, /*200 µA */
	    };
	    var entry = parseFloat(setting.attr("data-value"));
	    this.rdm = rdm[entry];
	    this.max = this.rdm.x
	    this.f = function (obj){
		return function(uifunc){
		    var ui = solve(uifunc, function(i){return i * obj.rdm.r});
		    obj.parent.setDisplay(obj.rdm.m * ui.u, obj.max, obj.rdm.d);
		}
	    }(this);
	}
    }
}

/**
 * @class Multimeter
 * implement some code to interact with a GUI implemented in SVG
 **/
class Multimeter {

    /**
     * @param ident the id attribute for the multimeter in the SVG image
     * @param elt a jQuery element, for example based on a "div"
     *   which contains at least a group with id attribute == "meter"
     **/
    constructor(ident, elt){
	/* pointer to the SVG group to draw a multimeter */
	var meter = elt.find("#"+ident);
	this.meter = meter;
	/* number of distinct settings reacable by the selector */
	this.nSettings = this.meter.find('[data-category="setting"]').length;
	/* pointer to the SVG group to draw the selector wheel */
	this.selector = this.meter.find("#selector")
	/* keep the inital angle of this wheel */
	this.transform0 = this.selector.attr("transform")
	var match=this.transform0.match(/rotate\(([.\d]+),\s*([.\d]+),\s*([.\d]+)\)/);
	this.selection = this.meter.find("#OFF");
	this.feature = new Feature(null, this);
	this.connectedTo = function(i){return -1e7*i}; // big R, null generator
	this.simulate();
	/* keep the coordinates of the selector's wheel center */
	this.selX = parseFloat(match[2]);
	this.selY = parseFloat(match[3]);
	/* initial settings for the display */
	this.display = this.meter.find("#display");
	/* give the mouse the shape of a pointer when it hovers */
	/* around the selector wheel */
	this.meter.find("[data-category='setting']").css({cursor: "pointer",});
	/* add every plug element to the nodes */
	var plugs = this.meter.find("[data-category='plug']");
	$.each(plugs, function(i, item){
	    item=$(item);
	    var ident = item.attr("id");
	    var n = new Node(ident, item, meter);
	    Nodes.push(n)
	});
    }

    /**
     * Define the external generator, as a function current --> voltage
     * @param fun may be a function with profile number -> number
     *   for example `function(i){return 220 * i;}` can be the right
     *   function for a 220 ohm resistor.
     **/
    connectTo(func){
	this.connectedTo = func;
	this.simulate();
    }
    
    simulate(){
	// launch the solve routine for this.feature with
	// the external generator.
	this.feature.f(this.connectedTo);
    }

    /**
     * Rotate the group who has id="selector"
     * @param angle the angle of rotation, in degree
     **/
    rotate_selector(angle){
	angle=parseFloat(angle)
	var transform = this.transform0.replace(
	    /rotate\(([.\d]+),\s*([.\d]+),\s*([.\d]+)\)/,
	    function(match, a, x, y){
		a=parseFloat(a);
		return "rotate("+(a+angle)+","+x+","+y+")";
	    }
	);
	this.selector.attr("transform", transform)
    }

    /**
     * Display a value
     * @param val the value to display; if it is a string like "" it
     *   blanks the display
     * @param max the limits of the values which can be displaied
     * @param dotpos the position of the decimal dot
     **/
    setDisplay(val, max, dotpos){
	var sign=1;
	var dt = this.meter.find("#display-text");
	dt.attr("style","font-family:lcdfont;font-size:50px");
	var dd = this.meter.find("#display-dot");
	dd.attr("style","font-family:lcdfont;font-size:50px");
	var ds = this.meter.find("#display-sign");
	ds.attr("style","font-family:lcdfont;font-size:40px");
	if (typeof(max) === "undefined") max=500;
	if (typeof(dotpos) === "undefined") dotpos = 6; // dot is not visible
	var text = "1"; // overflow by default
	if (typeof(val) == "string"){
	    text=""; // blank
	} else {
	    sign = Math.sign(val);
	    val=Math.abs(val);
	    if (val < max) {
		// round the value properly
		var pow10 = 1;
		for(var i = dotpos; i<4; i++) {
		    pow10 = 10*pow10;
		}
		var valrounded = Math.round(val*pow10);
		//make text always 4 characters long
		text = (""+valrounded);
		if(text == "0") sign = +1;
		// add zeroes to the left after the dot
		text = "000".substr(0,4-dotpos-text.length) + text;
		// add blanks before the dot
		text = "    ".substr(0,4-text.length) + text;
	    }
	}
	dt.html(text);
	if (text!="1" && dotpos < 4){
	    dd.html("     ".substring(0,dotpos)+"."); // no overflow
	} else {
	    dd.html("")
	}
	if (sign==-1){
	    ds.html("-");
	} else {
	    ds.html("");
	}
	
    }
    
}

/**
 * Inserts a SVG imge into some DIV element, then triggers an event svgReady
 *
 * @param url the URL of a SVG image
 * @param elt a jQuery element, for example based on a "div"
 **/
function insertSVG(url, elt){
    $.get(url).done(
	function(data){
	    svgDoc = data.documentElement;
	    elt.append(svgDoc);
	    elt.trigger('svgReady', {bubbles: true});
	}
    );
}

/**
 * Creates a closure to manage click events with a multimeter
 *
 * @param multimeter a Multimeter instance
 * @return a function to manage the click events
 **/
function mmClick(multimeter){
    return function (event){
	var target=$(event.target);
	var category = target.attr("data-category");
	if (category == "setting"){
	    var cx = parseFloat(target.attr("data-x"));
	    var cy = parseFloat(target.attr("data-y"));
	    var angle = 90 + Math.atan2(cy-multimeter.selY, cx-multimeter.selX) * 180 / Math.PI;
	    var step = 360/multimeter.nSettings;
	    angle = step * Math.round(angle/step);
	    var oldSelection = multimeter.selection;
	    multimeter.selection=target;
	    console.log(oldSelection.attr("id"), "==>", multimeter.selection.attr("id"));
	    multimeter.rotate_selector(angle);
	    multimeter.feature = new Feature(target, multimeter);
	    multimeter.simulate();
	} else  if (category == "plug"){
	    var cx = parseFloat(target.attr("cx"));
	    var cy = parseFloat(target.attr("cy"));
	    var n = Nodes.find( n => n.id == target.attr("id"));
	    if (linkingNode == null){
		// ready to create a link
		var NS = svgDoc.getAttribute('xmlns');
		var line = document.createElementNS(NS, 'line');
		var pt = svgDoc.createSVGPoint(), svgP;
		pt.x = event.clientX;
		pt.y = event.clientY;
		var svgP = pt.matrixTransform(svgDoc.getScreenCTM().inverse());
		line.setAttribute('x1', svgP.x);
		line.setAttribute('y1', svgP.y);
		line.setAttribute('x2', svgP.x+1);
		line.setAttribute('y2', svgP.y+1);
		line.setAttribute('stroke', 'magenta');
		line.setAttribute('stroke-width', '1');
		svgDoc.appendChild(line);
		linkingNode=n;
		linkingNode.line=line;
	    } else {
		// a link is being created : if we went back
		// to the starting node, cancel the link; otherwise
		// create the link.
		if (n == linkingNode){
		    //cancel the link
		    linkingNode.line.parentNode.removeChild(linkingNode.line);
		} else {
		    // keep the line, and make the link
		    linkingNode.links.push(n);
		    n.links.push(linkingNode);
		    var c = linkingNode.line.cloneNode(true);
		    linkingNode.line.parentNode.removeChild(linkingNode.line);
		    c.setAttribute("data-category", "wire");
		    c.setAttribute('stroke', 'cyan');
		    $(c).css({cursor: "crosshair"});
		    svgDoc.appendChild(c);
		    linkingNode.wires.push(c);
		    linkingNode.line = null;
		    linkingNode = null;
		}
	    }
	    svgDoc.addEventListener('mousemove', function(e) {
		if (linkingNode == null) return;
		var pt = svgDoc.createSVGPoint(), svgP;
		pt.x = e.clientX;
		pt.y = e.clientY;
		var svgP = pt.matrixTransform(svgDoc.getScreenCTM().inverse());
		linkingNode.line.setAttribute('x2', svgP.x);
		linkingNode.line.setAttribute('y2', svgP.y);
	    }, false);
	} else {
	    console.log("nope");
	}
    }
}

function schottky (i){
    /* this model gives 0.7 V for i = 7 mA */
    if(i<0) return 1e7*i; // big resistance
    else return 0.16*Math.log((i+1e-6)/1e-6); // Schottky model of a diode
}

function schottky_gen (i){
    /* this model gives 0.7 V for i = -7 mA */
    if(i>0) return -1e7*i; // big resistance, in generator mode
    else return 0.16*Math.log((-i+1e-6)/1e-6); // Schottky model of a diode
}

function demo(){
    /* prepare a jsxgraph board to plot debug data */
    board = JXG.JSXGraph.initBoard('box', {boundingbox:[-.001,2,0.008,-1], axis:true});
    var container = $("div#svgcontainer");
    // prepare a function to call when the SVG image is ready
    $("body").on("svgReady", function(){
	/* create an instance of Multimeter */
	var multimeter = new Multimeter("meter", container);
	/* bind a click handler to this instance */
	multimeter.meter.click(mmClick(multimeter));
	/* disclose the Multimeter instance at global level */
	mm = multimeter;
    });
    /* finally, fetch the SVG image */
    insertSVG("img/one_multimeter.svg", container);
}


window.onload = demo;
