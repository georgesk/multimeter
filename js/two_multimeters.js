/**
 * Global variable for the SVG drawing
 **/
var svgDoc;

/**
 * the global variables for two Multimeter instances, a power supply
 * and a resitor
 **/
var mm1, mm2, pw, r1;

/**
 * Global variable containing all the nodes
 **/
var Nodes=[];
var linkingNode=null; // the only node which is building a link at a time

/**
 * A board for jsxgraph plots
 **/
var board;

/**
 * audio stuff for the buzzer
 **/
var audioCtx = new(window.AudioContext || window.webkitAudioContext)();
var oscillator = null,  buzzing = false;

// Macro function plotter
function addCurve(board, func, atts) {
    var f = board.create('functiongraph', [func], atts);
    return f;
}
// Simplified plotting of function
function plot(func, atts) {
   if (atts==null) {
      return addCurve(board, func, {strokewidth:2});
   } else {
      return addCurve(board, func, atts);
   }    
}
function clearAll(board, imin, imax, vmin, vmax) {
    if (typeof(imin) == "undefined") imin = -1;
    if (typeof(imax) == "undefined") imax = 1;
    if (typeof(vmin) == "undefined") vmin = -10;
    if (typeof(vmax) == "undefined") vmax = 10;
    JXG.JSXGraph.freeBoard(board);
    board = JXG.JSXGraph.initBoard('box', {boundingbox:[imin,vmax,imax,vmin], axis:true});
    return board;
}


/**
 * decimal logarithm
 **/
function log10(x){
    return Math.log(x)/Math.log(10);
}

/**
 * number of digits of a number before its decimal dot
 **/
function beforeDot(x){
    return Math.floor(log10(x))+1
}

/**
 * @class Node
 * to implement electrical nodes. Each node has a potential, and
 * the sum of currents arriving in a node is zero (in permanent regime)
 * 
 * Nodes can be linked to each other, and they have a property "linking"
 * which is true when someone is creating a link originating from this node.
 **/
class Node {
    /**
     * Parameters of the constructor:
     * @param name the name of a plug
     * @param el a jQuery element in the SVG drawing
     * @param parent a link to a parent objet; the couple (parent, name) should
     *   be unique.
     **/
    constructor(name, el, parent){
	this.name = name;
	this.el = el;
	this.el.css({cursor: "pointer"});
	this.parent = parent;
	this.links = []; // list of nodes linked by a wire
	this.line = null;
	this.wires = []; // list of wires originated here 
    }

    /**
     * Provide a unique name for the node
     **/
    unique_name(){
	// gets the SVG id attribute of the parent, which should be unique
	// followed by the id member of the node, which may be more generic
	return this.parent.attr("id") + "/" + this.name;
    }

    /**
     * Provide a readable HTML presentation of the node
     **/
    html(){
	var parent_name = this.parent.attr("id");
	var linkedNodes=[]
	this.links.forEach(function(n){
	    linkedNodes.push(n.unique_name())
	});
	return "\
<table>\
<tr><th colspan='2'>"+this.unique_name()+"</td></tr>\
<tr><th>links</th><td>"+linkedNodes.join(",<br/>")+"</td></tr>\
</table>\
";
    }
}

/**
 * Show the list of nodes in a div
 */
function showNodes(){
    var nodesdiv = $("#les_noeuds");
    nodesdiv.empty();
    function show(n){
	var d=$("<div>");
	d.html(n.html()).addClass("ilb");
	nodesdiv.append(d);
    }
    Nodes.forEach(n => show(n))
}

/**
 * Interactions when somebody is attempting to create a wire
 * @param target jQuery element, whose attribute data-category should be "plug"
 **/
function wiring(target){
    var cx = parseFloat(target.attr("cx"));
    var cy = parseFloat(target.attr("cy"));
    var parent_device = target.parents('[data-category="device"]').first();
    var un = parent_device.attr("id") + "/" + target.attr("data-name");
    console.log("un=", un);
    var n = Nodes.find( n => n.unique_name() == un);
    if (linkingNode == null){
	// ready to create a link
	var NS = svgDoc.getAttribute('xmlns');
	var line = document.createElementNS(NS, 'line');
	var pt = svgDoc.createSVGPoint(), svgP;
	pt.x = event.clientX;
	pt.y = event.clientY;
	svgP = pt.matrixTransform(svgDoc.getScreenCTM().inverse());
	line.setAttribute('x1', svgP.x);
	line.setAttribute('y1', svgP.y);
	line.setAttribute('x2', svgP.x+1);
	line.setAttribute('y2', svgP.y+1);
	line.setAttribute('stroke', 'magenta');
	line.setAttribute('stroke-width', '1');
	svgDoc.appendChild(line);
	linkingNode=n;
	linkingNode.line=line;
    } else {
	// a link is being created : if we went back
	// to the starting node, cancel the link; otherwise
	// create the link.
	if (n == linkingNode){
	    //cancel the link and forget linkingNode
	    linkingNode.line.parentNode.removeChild(linkingNode.line);
	    linkingNode = null;
	} else {
	    // keep the line, and make the link
	    linkingNode.links.push(n);
	    n.links.push(linkingNode);
	    var c = linkingNode.line.cloneNode(true);
	    linkingNode.line.parentNode.removeChild(linkingNode.line);
	    c.setAttribute("data-category", "wire");
	    c.setAttribute('stroke', 'cyan');
	    $(c).css({cursor: "crosshair"});
	    svgDoc.appendChild(c);
	    linkingNode.wires.push(c);
	    linkingNode.line = null;
	    linkingNode = null;
	    showNodes();
	}
    }
    svgDoc.addEventListener('mousemove', function(e) {
	if (linkingNode == null) return;
	var pt = svgDoc.createSVGPoint(), svgP;
	pt.x = e.clientX;
	pt.y = e.clientY;
	var svgP = pt.matrixTransform(svgDoc.getScreenCTM().inverse());
	linkingNode.line.setAttribute('x2', svgP.x);
	linkingNode.line.setAttribute('y2', svgP.y);
    }, false);
}

/**
 * Interactions when somebody is turning the multimeter's selector
 * @param target jQuery element, whose attribute data-category should be "plug"
 * @param mutimeter a Multimeter instance
 **/
function turnSelector(target, multimeter){
    var cx = parseFloat(target.attr("data-x"));
    var cy = parseFloat(target.attr("data-y"));
    var angle = 90 + Math.atan2(cy-multimeter.selY, cx-multimeter.selX) * 180 / Math.PI;
    var step = 360/multimeter.nSettings;
    angle = step * Math.round(angle/step);
    var oldSelection = multimeter.selection;
    multimeter.selection=target;
    console.log(oldSelection.attr("data-name"), "==>", multimeter.selection.attr("data-name"));
    multimeter.rotate_selector(angle);
    multimeter.feature = new Feature(target, multimeter);
    multimeter.simulate();
}

/**
 * Interactions when somebody is turning the power supply's button
 * @param target jQuery element, whose attribute data-category should be "plug"
 * @param power a Power instance
 **/
function turnButton(event, power){
    var pt = svgDoc.createSVGPoint(), svgP, svgCenter;
    pt.x = event.clientX;
    pt.y = event.clientY;
    var svgP = pt.matrixTransform(svgDoc.getScreenCTM().inverse());
    var distance = Math.sqrt((svgP.x-power.center.x)**2+(svgP.y-power.center.y)**2);
    if (distance>10 && distance < 30){
	// the mouse cursor is near the perimeter of the selector
	var angle = Math.atan2((svgP.y-power.center.y), (svgP.x-power.center.x)) / Math.PI * 180 + 90;
	if (angle > 180) angle -= 360;
	if (Math.abs(angle) < 135){
	    // the selector has to turn.
	    var g = power.selector.parent();
	    g.attr({transform: "rotate("+angle+","+power.rotatecenter.x+","+power.rotatecenter.y+")"});
	    power.backlight.h = -(135+angle)/270*40;
	    power.backlight.elt.attr({
		d: "m "+power.backlight.x+","+power.backlight.y+" h "+power.backlight.w+" v "+power.backlight.h+" h -"+power.backlight.w+" z"
	    });
	    power.voltage = angle/135*15;
	    // emit a signal to both multimeters
	    mm1.meter.trigger('voltageChanged', {bubbles: true});
	    mm2.meter.trigger('voltageChanged', {bubbles: true});
	}
    }
}

/**
 * @class UI
 * a class to keep a voltage (u) and a current (i)
 **/
class UI {
    constructor (u,i){
	this.voltage=u;
	this.current=i;
    }

    get u(){
	return this.voltage;
    }

    get i(){
	return this.current;
    }
}

/**
 * solve electrical problems where two dipoles are connected, each one
 * described by its characteristic ui function, i.e. the function which
 * yelds the voltage u when is given the current i.
 *
 * param uifunc1 a function which yields u, given i
 * param uifunc1 another function which yields u, given i
 * @retun the solution when u and i are the same for both functions
 */
function solve( uifunc1, uifunc2){
    var uifunc = function(i){return uifunc1(i) - uifunc2(i)}
    var imax = 11, imin = -10;
    var epsilon = 1e-14;
    if ((uifunc(imax) * uifunc(imin)) > 0){
	// This is an error condition : voltages are the same sign
	// for both imin and imax
	// throw "Cannot solve the equations."
	board = clearAll(board);
	c1=plot(function(x){return uifunc1(x)});
	c2=plot(function(x){return uifunc2(x)},{strokecolor:'red', dash:1});
	return new UI(1000, 10);
    }
    var i;
    while ((imax - imin) > epsilon){
	i = (imax+imin)/2;
	if ((uifunc(imax) * uifunc(i)) >= 0) {
	    // the voltages have the same signs for imax and i; change imax
	    imax = i;
	} else {
	    // the voltages have opposite signs for imax and i; change imin
	    imin = i;
	}
    }
    var ui = new UI(uifunc1(i), i);
    var imin = -1e-4, imax = 1e-4, vmin = -1e-3, vmax = 1e-3;
    if (Math.abs(ui.i) > imax){
	imin = -1.5 * Math.abs(ui.i);
	imax = -imin;
    }
    if (Math.abs(ui.u) > vmax){
	var vmin = -1.5 * Math.abs(ui.u);
	var vmax = -vmin;
    }
    board = clearAll(board, imin, imax, vmin, vmax);
    c1=plot(function(x){return uifunc1(x)});
    c2=plot(function(x){return uifunc2(x)},{strokecolor:'red', dash:1});
    return ui;
}

/**
 * @class Feature
 * description of a multimeter's feature when some function is selected.
 *
 * The multimeter is considered as a resistor (finite or not) which
 * can be connected to an external electric setup by two points.
 * Eventually, it can have an internal source of voltage, for example
 * when used to test external resistors, diode junctions or electric.
 * continuity.
 **/
class Feature {

    /**
     * @param setting a jQuery element from the SVG tree, with
     *   attribute data-category == "setting"
     * @param parent the parent multimeter
     **/
    constructor(setting, parent){
	if (buzzing){
	    oscillator.stop(); // stop any buzzer
	    buzzing= false;
	}
	/* default */
	this.r = 5e7;
	this.E = 0;
	this.parent = parent;
	this.max = 500;
	this.f = function(multimeter){
	    return function(){
		multimeter.setDisplay("");
	    }
	}(this.parent);
	var unit = null, diode = false, buzzer = false;
	if(setting) {
	    unit = setting.attr("data-unit");
	    diode = setting.attr("data-name") == "diode";
	    buzzer = setting.attr("data-name") == "buzzer";
	}
	/*************** BUZZER *****************************/
	if (buzzer){
	    this.rdm = { r: 1e9,   d:3, m: 1e3, x: 200 };  /*200   ohm */
	    this.max = this.rdm.x
	    this.E = 1e6; // fake big value to make easily a current source
	    this.f = function (obj){
		return function(uifunc){
		    var ui = solve(uifunc, function(i){return obj.E + i * obj.rdm.r});
		    obj.parent.setDisplay(obj.rdm.m * ui.u, obj.max, obj.rdm.d);
		    if (Math.abs(ui.u) < 0.2) {
			// low resistance, buzz!
			if (!buzzing){
			    oscillator = audioCtx.createOscillator();
			    oscillator.type = 'square';
			    oscillator.frequency.value = 880;
			    oscillator.connect(audioCtx.destination);
			    oscillator.start();
			    buzzing = true;
			}
		    } else {
			// high resistance, don't buzz
			if (buzzing){
			    oscillator.stop(); // stop any buzzer
			    buzzing= false;
			}
		    }
		}
	    }(this);
	}
	/*************** DIODE *****************************/
	if (diode){
	    this.E = 9;      // emf of the battery
	    this.r = 1e5     // 100 kOhm, current near 100 µA
	    this.max = 2000  // range = 2000 mV
	    this.dotpos = 4
	    this.f = function (obj){
		return function(uifunc){
		    var ui = solve(uifunc, function(i){return obj.E + i * obj.r});
		    obj.parent.setDisplay(1000*ui.u, obj.max, obj.dotpos);
		}
	    }(this);
	    
	}
	/*************** VOLTMETER *****************************/
	if (! diode && unit == "V"){
	    var rdm={
		/* r resistance, d dot position, m multiplicator, x max */
		500: { r: 1e7,  d:4, m: 1e0, x: 500 },  /*500 V */
		200: { r: 1e7,  d:3, m: 1e0, x: 200}, /*200 V */
		20:  { r: 1e7,  d:2, m: 1e0, x: 20  }, /*20 V */
		2:   { r: 1e7,  d:4, m: 1e3, x: 2000 }, /*2000 mV */
		0.2: { r: 1e7,  d:3, m: 1e3, x: 200}, /*200 mV */
	    };
	    var entry = parseFloat(setting.attr("data-value"));
	    this.rdm = rdm[entry];
	    this.max = this.rdm.x
	    if (setting.attr("data-acdc") == "dc") {
		//direct current
		this.f = function (obj){
		    return function(uifunc){
			var ui = solve(uifunc, function(i){return i * obj.rdm.r});
			obj.parent.setDisplay(obj.rdm.m * ui.u, obj.max, obj.rdm.d);
		    }
		}(this);
	    } else {
		//alternative current so the external function uifunc
		// is distorted
		this.f = function (obj){
		    return function(uifunc){
			var uifunc2 = function(i){
			    var urectified = Math.abs(uifunc(i));
			    if (urectified > 0.7) urectified -= 0.7;
			    else urectified = 0;
			    return urectified*Math.sqrt(2);
			}
			var ui = solve(uifunc2, function(i){return obj.E + i * obj.r});
			obj.parent.setDisplay(ui.u, obj.max, obj.dotpos);
		    }
		}(this);
	    }
	}
	/*************** OHMMETER *****************************/
	if (unit == "ohm"){
	    var rdm={
		/* r resistance, d dot position, m multiplicator, x max */
		200:     { r: 1e9,   d:3, m: 1e3, x: 200 },  /*200   ohm */
		2000:    { r: 1e10,  d:4, m: 1e4, x: 2000}, /*2000  ohm */
		20000:   { r: 1e11,  d:2, m: 1e2, x: 20  }, /*20k   ohm */
		200000:  { r: 1e12,  d:3, m: 1e3, x: 200 }, /*200k  ohm */
		2000000: { r: 1e13,  d:4, m: 1e4, x: 2000}, /*2000k ohm */
	    };
	    var entry = parseFloat(setting.attr("data-value"));
	    this.rdm = rdm[entry];
	    this.max = this.rdm.x
	    this.E = 1e6; // fake big value to make easily a current source
	    this.f = function (obj){
		return function(uifunc){
		    var ui = solve(uifunc, function(i){return obj.E + i * obj.rdm.r});
		    obj.parent.setDisplay(obj.rdm.m * ui.u, obj.max, obj.rdm.d);
		}
	    }(this);
	}
	/*************** AMMETER *****************************/
	if (unit == "A"){
	    var rdm={
		/* r shunt resistance, d dot position, m multiplicator, x max */
		10:   { r: 1e-3, d:2, m: 1e4, x: 10  }, /*10 A */
		2e-1: { r: 1e-1, d:3, m: 1e4, x: 200 }, /*200  mA */
		2e-2: { r: 1e0 , d:2, m: 1e3, x: 20  }, /*20   mA */
		2e-3: { r: 1e1 , d:4, m: 1e5, x: 2000}, /*2000  µA */
		2e-4: { r: 1e2 , d:3, m: 1e4, x: 200 }, /*200 µA */
	    };
	    var entry = parseFloat(setting.attr("data-value"));
	    this.rdm = rdm[entry];
	    this.max = this.rdm.x
	    this.f = function (obj){
		return function(uifunc){
		    var ui = solve(uifunc, function(i){return i * obj.rdm.r});
		    obj.parent.setDisplay(obj.rdm.m * ui.u, obj.max, obj.rdm.d);
		}
	    }(this);
	}
    }
}

/**
 * Create Node instances for plugs in an element
 * @param elt a jQuery element cotining childred with attribute 
 * data-category like "plug"
 **/
function createPlugNodes(elt){
    var plugs = elt.find("[data-category='plug']");
    $.each(plugs, function(i, item){
	item=$(item);
	var name = item.attr("data-name");
	var n = new Node(name, item, elt);
	Nodes.push(n)
    });
}

const colorcode = [
    /** 0, black **/   {r:0, g:0, b:0},
    /** 1, marroon **/ {r:138, g:59, b:1},
    /** 2, red **/     {r:255, g:0, b:0},
    /** 3, orange **/  {r:240, g:172, b:15},
    /** 4, yellow **/  {r:245, g:245, b:10},
    /** 5, green **/   {r:51, g:245, b:10},
    /** 6, blue **/    {r:8, g:152, b:211},
    /** 7, violet **/  {r:212, g:5, b:215},
    /** 8, grey **/    {r:133, g:133, b:133},
    /** 9, white **/   {r:249, g:249, b:249},
]

/**
 * @class Resistor
 * implement some code to interact with a GUI implemented in SVG
 **/
class Resistor {
    /**
     * @param ident the id attribute for the multimeter in the SVG image
     * @param elt a jQuery element, for example based on a "div"
     *   which contains at least a group with an id attribute like "resistor"
     * @param val a value in Ohm; defaults to 1000 when undefined
     * @param label true if we want to display the value as a text
     **/
    constructor(ident, elt, val, label){
	/* pointer to the SVG group to draw a multimeter */
	var res = elt.find("#"+ident);
	this.resistor = res;
	this.resistor.on("click", resClick(this));
	createPlugNodes(res);
	if (typeof(val) === "undefined") val = 1000;
	var value = val+ 0.1; // prevent arithmetic mistakes if r = 1000
	var pow = Math.floor(log10(value));
	var s1 = Math.floor(value/10**pow);
	var s2 = Math.floor(value/10**(pow-1)) - 10 * s1;
	var stripe1 = res.find('[data-name="stripe1"]')
	var stripe2 = res.find('[data-name="stripe2"]')
	var stripe3 = res.find('[data-name="stripe3"]')
	var c;
	c = colorcode[s1];
	stripe1.css({fill: "rgb("+c.r+","+c.g+","+c.b+")"})
 	c = colorcode[s2];
	stripe2.css({fill: "rgb("+c.r+","+c.g+","+c.b+")"})
 	c = colorcode[pow-1];
	stripe3.css({fill: "rgb("+c.r+","+c.g+","+c.b+")"})
	var textspan = res.find('[data-name="valtext"]');
	if (typeof(label) !== "undefined" && label){
	    textspan.html(val + " Ω ± 5%")
	} else {
	    textspan.html(" ");
	}
    }
}

/**
 * @class Multimeter
 * implement some code to interact with a GUI implemented in SVG
 **/
class Multimeter {

    /**
     * @param ident the id attribute for the multimeter in the SVG image
     * @param elt a jQuery element, for example based on a "div"
     *   which contains at least a group with an id attribute like "meter"
     **/
    constructor(ident, elt){
	/* pointer to the SVG group to draw a multimeter */
	var meter = elt.find("#"+ident);
	this.meter = meter;
	this.feature = new Feature(null, this);
	// bind event listeners
	this.meter.on("click", mmClick(this));
	this.meter.on("click", mmSim(this));
	this.meter.on("voltageChanged",mmSim(this));
	/* number of distinct settings reachable by the selector */
	this.nSettings = this.meter.find('[data-category="setting"]').length;
	/* pointer to the SVG group to draw the selector wheel */
	this.selector = this.meter.find("[data-name='selector']")
	/* keep the inital angle of this wheel */
	this.transform0 = this.selector.attr("transform")
	var match=this.transform0.match(/rotate\(([.\d]+),\s*([.\d]+),\s*([.\d]+)\)/);
	this.selection = this.meter.find("[data-name='OFF']");
	this.connectedTo = function(i){return -1e7*i}; // big R, null generator
	this.simulate();
	/* keep the coordinates of the selector's wheel center */
	this.selX = parseFloat(match[2]);
	this.selY = parseFloat(match[3]);
	/* initial settings for the display */
	this.display = this.meter.find("[data-name='display']");
	/* give the mouse the shape of a pointer when it hovers */
	/* around the selector wheel */
	this.meter.find("[data-category='setting']").css({cursor: "pointer",});
	createPlugNodes(meter);
    }

    /**
     * Define the external generator, as a function current --> voltage
     * @param fun may be a function with profile number -> number
     *   for example `function(i){return 220 * i;}` can be the right
     *   function for a 220 ohm resistor.
     **/
    connectTo(func){
	this.connectedTo = func;
	this.simulate();
    }
    
    /**
     * Rotate the group who has id="selector"
     * @param angle the angle of rotation, in degree
     **/
    rotate_selector(angle){
	angle=parseFloat(angle)
	var transform = this.transform0.replace(
	    /rotate\(([.\d]+),\s*([.\d]+),\s*([.\d]+)\)/,
	    function(match, a, x, y){
		a=parseFloat(a);
		return "rotate("+(a+angle)+","+x+","+y+")";
	    }
	);
	this.selector.attr("transform", transform)
    }

    /**
     * Display a value
     * @param val the value to display; if it is a string like "" it
     *   blanks the display
     * @param max the limits of the values which can be displaied
     * @param dotpos the position of the decimal dot
     **/
    setDisplay(val, max, dotpos){
	var sign=1;
	var dt = this.meter.find("[data-name='display-text']");
	dt.attr("style","font-family:lcdfont;font-size:50px");
	var dd = this.meter.find("[data-name='display-dot']");
	dd.attr("style","font-family:lcdfont;font-size:50px");
	var ds = this.meter.find("[data-name='display-sign']");
	ds.attr("style","font-family:lcdfont;font-size:40px");
	if (typeof(max) === "undefined") max=500;
	if (typeof(dotpos) === "undefined") dotpos = 6; // dot is not visible
	var text = "1"; // overflow by default
	if (typeof(val) == "string"){
	    text=""; // blank
	} else {
	    sign = Math.sign(val);
	    val=Math.abs(val);
	    if (val < max) {
		// round the value properly
		var pow10 = 1;
		for(var i = dotpos; i<4; i++) {
		    pow10 = 10*pow10;
		}
		var valrounded = Math.round(val*pow10);
		//make text always 4 characters long
		text = (""+valrounded);
		if(text == "0") sign = +1;
		// add zeroes to the left after the dot
		text = "000".substr(0,4-dotpos-text.length) + text;
		// add blanks before the dot
		text = "    ".substr(0,4-text.length) + text;
	    }
	}
	dt.html(text);
	if (text!="1" && dotpos < 4){
	    dd.html("     ".substring(0,dotpos)+"."); // no overflow
	} else {
	    dd.html("")
	}
	if (sign==-1){
	    ds.html("-");
	} else {
	    ds.html("");
	}
	
    }
    
    simulate(){
	// launch the solve routine for this.feature with
	// the external generator.
	this.feature.f(this.connectedTo);
    }

}

/**
 * @class Power
 *  to implement DC power supplies (voltage sources)
 **/
class Power {

    /**
     * @param ident the id attribute for the multimeter in the SVG image
     * @param elt a jQuery element, for example based on a "div"
     *   which contains at least a group with an id attribute like "power"
     **/
    constructor(ident, elt){
	var power = elt.find("#"+ident);
	this.power = power;
	this.power.on("click", pwClick(this));
	this.power.on("mousemove", pwClick(this));
	this.vmin = parseFloat(power.attr("data-vmin"));
	this.vmax = parseFloat(power.attr("data-vmax"));
	this.voltage = 0.0;
	this.selector = this.power.find("[data-name='selector']");
	this.display = this.power.find("[data-name='display']");
	// compute the SVG coordinates of the box center
	var groups=this.selector.parents("g");
	var expr=/translate\(([.\-\d]+),([.\-\d]+)\)/;
	var m;
	var cx = 0, cy = 0;
	groups.each(function(i,item){
	    m =$(item).attr("transform").match(expr);
	    if (m){
		cx+=parseFloat(m[1]);
		cy+=parseFloat(m[2]);
	    }
	})
	var bb=this.power[0].getBBox();
	// center for interactions with mouse clicks
	this.center = {
	    x: bb.x + bb.width/2  + cx,
	    y: bb.y + bb.height/2 + cy
	};
	var g = this.selector.parent();
	expr = /rotate\(([.\-\d]+),\s*([.\-\d]+),\s*([.\-\d]+)\)/;
	m = g.attr("transform").match(expr);
	// center to modify this.selector.parent's rotation
	this.rotatecenter = {
	    x: parseFloat(m[2]),
	    y: parseFloat(m[3])
	};
	var backlight = this.power.find("[data-name='back-light']");
	expr=/m ([.\d]+),([.\d]+) h ([.\d]+) v ([.\-\d]+) .*/;
	m=backlight.attr("d").match(expr);
	this.backlight={
	    elt: backlight,
	    x: parseFloat(m[1]),
	    y: parseFloat(m[2]),
	    w: parseFloat(m[3]),
	    h: parseFloat(m[4])
	};
	createPlugNodes(power);
    }

    /**
     * Getter to have the voltage
     * @return the current voltage
     **/
    get u (){
	return this.voltage;
    }
}

/**
 * Creates a closure to manage click events with a power supply
 *
 * @param power a Power instance
 * @return a function to manage the click events
 **/
function pwClick(power){
    return function(event){
	if (event.type == "click"){
	    var target=$(event.target);
	    var category = target.attr("data-category");
	    if (category == "plug"){
		wiring(target);
	    }
	}
	// forget the event if the mouse is hovering and the
	// left button is not pressed
	if (event.type == "mousemove" && event.buttons != 1) return true;
	// still there ? so the event can be "click" or "mousemove"
	// with left button down.
	turnButton(event, power)
    }
}

/**
 * Inserts a SVG image into some DIV element, then triggers an event svgReady
 *
 * @param url the URL of a SVG image
 * @param elt a jQuery element, for example based on a "div"
 **/
function insertSVG(url, elt){
    $.get(url).done(
	function(data){
	    svgDoc = data.documentElement;
	    elt.append(svgDoc);
	    elt.trigger('svgReady', {bubbles: true});
	}
    );
}

/**
 * Creates a closure to manage voltageChanged events with a multimeter
 *
 * @param multimeter a Multimeter instance
 * @return a function to manage the voltageChanged events
 **/
function mmSim(multimeter){
    return function(event){
	multimeter.simulate();
	return true;
    };
}


/**
 * Creates a closure to manage click events with a resistor
 *
 * @param resistor a Resistor instance
 * @return a function to manage the click events
 **/
function resClick(resistor){
    return function(event){
	var target=$(event.target);
	var category = target.attr("data-category");
	if (category == "plug"){
	    wiring(target);
	}
	return false;
     }
}

/**
 * Creates a closure to manage click events with a multimeter
 *
 * @param multimeter a Multimeter instance
 * @return a function to manage the click events
 **/
function mmClick(multimeter){
    return function (event){
	var target=$(event.target);
	var category = target.attr("data-category");
	if (category == "setting"){
	    turnSelector(target, multimeter);
	} else  if (category == "plug"){
	    wiring(target);
	}
	return false;
    }
}

function schottky (i){
    /* this model gives 0.7 V for i = 7 mA */
    if(i<0) return 1e7*i; // big resistance
    else return 0.16*Math.log((i+1e-6)/1e-6); // Schottky model of a diode
}

function schottky_gen (i){
    /* this model gives 0.7 V for i = -7 mA */
    if(i>0) return -1e7*i; // big resistance, in generator mode
    else return 0.16*Math.log((-i+1e-6)/1e-6); // Schottky model of a diode
}

function demo(){
    /* prepare a jsxgraph board to plot debug data */
    board = JXG.JSXGraph.initBoard('box', {boundingbox:[-.001,2,0.008,-1], axis:true});
    var container = $("div#svgcontainer");
    // prepare a function to call when the SVG image is ready
    $("body").on("svgReady", function(){
	/* create instances of Multimeters and Power */
	var multimeter1 = new Multimeter("meter1", container);
	var multimeter2 = new Multimeter("meter2", container);
	var power = new Power("power", container);
	var resistor1 = new Resistor("resistor", container, 47000 /*ohm*/, true);
	/* disclose the instances at global level */
	mm1 = multimeter1;
	mm2 = multimeter2;
	pw = power;
	r1 = resistor1;
    });
    /* finally, fetch the SVG image */
    insertSVG("img/two_multimeters.svg", container);
}


window.onload = demo;
