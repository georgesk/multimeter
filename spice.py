"""
Un micro-simulateur Spice, fonctionnant en courant continu, pour des
composants linéaires seulement : résistances et générateurs.
"""
import sys, re
import networkx as nx
import numpy as np
import unittest

class Norton:
    """
    implémente un générateur de Norton, donné par une source de courant
    et une conductnce en parallèle avec celle-ci

    Paramètres du constructeur :

    :param i: courant de court-circuit
    :param g: conductance
    :param plus: désigne le nœud "plus" où le courant arrive, None par défaut.
       ce paramètre est important pour les dipôles qui ont un sens comme
       les générateurs.
    """

    def __init__(self, i, g, plus = None):
        self.i = i
        self.g = g
        self.plus = plus
        return

    def __repr__(self):
        gen = ""
        if self.plus:
            gen = f"i = {self.i:4.2e} (en {self.plus}), "
        return f"Norton({gen}g = {self.g:4.2e})"

    @staticmethod
    def fromRes(r):
        """
        :param r: une valeur de résistance
        :return: une instance de Nortan basée sur la valeur de résistance
        """
        return Norton(0, 1/r)

    @staticmethod
    def fromVolt(u, r = 1e-6, plus=None):
        """
        :param u: une valeur de tension
        :param r: une résistance interne (un micro-ohm par défaut)
        :param plus: désigne un nœud, fait une erreur si on ne définit
           pas le paramètre.
        :return: une instance de Nortan basée sur la valeur de tension
          on simule une source de tension avec une résistance interne
        """
        if plus == None:
            raise Exception("le paramètre obligatoire 'plus' doit désigner un nœud")
        return Norton(u/r, 1/r, plus)

class CircuitGraph(nx.MultiDiGraph):
    """
    Implémentation d'un circuit sous forme d'un mutigraphe dirigé.

    Paramètres du constructeur:

    :param fname: le nom d'un fichier texte (optionnel)
    """

    def __init__(self, fname=""):
        nx.MultiDiGraph.__init__(self)
        if fname:
            with open(fname) as infile:
                lines=infile.readlines()
            assert lines[-1].lower() == ".end\n"
            lines = lines[1:-1]
            for l in lines:
                l=l.lower()
                if l[0] == "r":
                    # une résistance
                    nom, noeud1, noeud2, valeur = re.split(r"\s+", l.strip())
                    noeud1 = int(noeud1)
                    noeud2 = int(noeud2)
                    m=re.match(r"([.\d]+)(k|M)?", valeur)
                    valeur=float(m.group(1))
                    if m.group(2) == "k":
                        valeur *= 1e3
                    elif m.group(2) == "M":
                        valeur *= 1e6
                    self.add_edge(noeud2, noeud1, norton=Norton.fromRes(valeur))
                elif l[0] == "v":
                    nom, noeud1, noeud2, acdc, valeur = re.split(r"\s+", l.strip())
                    noeud1 = int(noeud1)
                    noeud2 = int(noeud2)
                    valeur = float(valeur)
                    self.add_edge(noeud2, noeud1, norton=Norton.fromVolt(valeur, plus=noeud1))
        return

    def __str__(self):
        """
        Affiche le contenu du circuit
        """
        return nx.nx_agraph.to_agraph(self).string()

    def g_matrix__(self):
        """
        Crée la matrice complète des conductivités du circuit.

        Cette matrice, de n x n éléments a un déterminant nul par
        construction, en effet on peut fixer arbitrairement un
        potentiel, et tous les autres en dépendent, la solution
        n'est donc pas unique.

        :return: une matrice
        :rtype: np.array
        """

        def sum_g(i,j):
            """
            additionne toutes les conductances entre les nœuds i et j
            :param i: un nœud
            :param j: un noeud
            :return: la somme de toutes les conductances entre les nœuds
            """
            sum=0
            if i == j :
                return 0
            data = self.get_edge_data(i, j)
            if data:
                for key, attr in data.items():
                    sum += attr["norton"].g
            data = self.get_edge_data(j, i)
            if data:
                for key, attr in data.items():
                    sum += attr["norton"].g
            return sum
        
        num=nx.number_of_nodes(self)
        result=np.zeros((num, num))
        for ni in nx.nodes(self):
            for nj in nx.nodes(self):
                if ni != nj:
                    # opposé de la somme de tous les g entre ni et nj
                    result[ni][nj] = - sum_g(ni, nj)
                else:
                    # on la somme de tous les g connectés à i==j
                    result[ni][nj] = sum([sum_g(ni, nk) for nk in nx.nodes(self)])
        return result
        
    def g_matrix(self):
        """
        Crée la matrice réduite des conductivités du circuit.

        Cette matrice ne contient plus que (n-1) x (n-1) valeurs,
        elle s'obtient à partir de la matrice complète en retirant
        les valeurs de la première ligne (pour le nœud 0, la masse),
        et celles de la première colonne (pour le même nœud)

        :return: une matrice 2D
        :rtype: np.array
        """
        return self.g_matrix__()[1:,1:]

    def i_vector(self):
        """
        Crée le vecteur des courants pour tous les nœuds sauf
        la masse.

        ce vecteur contient (n-1) valeurs, chaque position
        représente la somme des courants entrant dans le nœud
        """
        def sum_i(n):
            """
            additionne toutes courants entrants au nœud n
            :param n: un nœud
            :return: la somme de tous les courants entrants dans ce nœud
            """
            sum = 0
            for ni in nx.nodes(self):
                data = self.get_edge_data(ni, n)
                if data:
                    for key, attr in data.items():
                        norton=attr["norton"]
                        if norton.plus == n:
                            sum += norton.i
                        else:
                            sum -= norton.i
                data = self.get_edge_data(n, ni)
                if data:
                    for key, attr in data.items():
                        norton=attr["norton"]
                        if norton.plus == n:
                            sum += norton.i
                        else:
                            sum -= norton.i
            return sum
        num=nx.number_of_nodes(self)
        result = np.zeros(num-1)
        for i in range(1,num):
            result[i-1] = sum_i(i)
        return result

    def solve(self):
        """
        Trouve le vecteur des potentiels V1, V2 ... solution du circuit donné
        :return: un vecteur de potentiels
        :rtype: np.array
        """
        return np.linalg.solve(self.g_matrix(), self.i_vector())

class TestCircuitSolutions(unittest.TestCase):

    def check_solution(self, testFile, corrFile):
        solution = np.round(CircuitGraph(testFile).solve(),3)
        with open(corrFile) as infile:
            correction = infile.read()
        self.assertEqual(str(solution), correction.strip())
        return

    def test_0(self):
        fichiers=[
            ("tests/test0.cir", "tests/corr0.txt"),
            ("tests/test1.cir", "tests/corr1.txt"),
        ]
        for t,c in fichiers:
            self.check_solution(t,c)
        return

def demo (fname):
    """
    fait une démonstration des algorithmes appliqués à un circuit,
    renvoie du texte.
    :param fname: le nom d'un fichier .cir qui est analysé
    """
    G = CircuitGraph(fname)
    mat = G.g_matrix()
    cour = G.i_vector()
    return f"""\
matrice des conductivités :
{mat}
déterminant :
{np.linalg.det(mat)}
solution : {np.round(G.solve(),3)}"""
    
    
if __name__=="__main__":
    print(demo(sys.argv[1]))
