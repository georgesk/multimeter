from xml.dom import minidom
import re

# regular expression to match automatic keywords used as "id" attribute
# by Inkscape
auto_inkscape_attribute = re.compile (r"^rect.*$|^text.*$|^tspan.*$|^g\d+.*$|^path.*$|^layer.*$|^metadata.*$|^def.*$|^stop.*$|^linear.*$|^radial.*$|namedview.*$|^svg.*$|^circle[-\d]+$|^group[-\d]+$")

def recurseChildren(el,l,auto_attribute = auto_inkscape_attribute):
    """
    Recursively adds non-automatic keywords used as "id" attribute
    :param el: an element of a XML DOM tree
    :param l: a list to append found keywords
    :param auto_attribute: a compiled regular expression to mach
      automatic attributes; defaults to auto_inkscape_attribute which
      matches automatic attributes from Inkscape
    """
    for n in el.childNodes:
        if n.attributes and "id" in n.attributes:
            attr = n.getAttribute("id")
            if not auto_attribute.match(attr):
                l.append(attr)
        recurseChildren(n,l,auto_attribute)
    return

def getKeywords(xml):
    """
    :param xml: the name of an XML file, or an xml.dom instance
    :type  xml: str or xml.dom.Document
    :return: a list of all non-automatic keywords used as "id" attribute
      in the tree of an XML DOM object.
    """
    if type(xml) == str:
        doc=minidom.parse(open(fname))
    else:
        doc=xml
    ids=[]
    recurseChildren(doc, ids)
    return ids

if __name__=="__main__":
    print(getKeywords("multimetre.svg"))
    
