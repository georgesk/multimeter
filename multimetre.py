"""
Collection of programs to  use a virtual multimeter
"""
from xml.dom import minidom

class multimeterElement:
    """
    Representation of an element of a multimeter available in the GUI

    Parameters of the constructor:

    :param name: a keyword which is used as an "id" attribute inside the SVG
      file describing the multimeter
    :param comment: a free coment string
    :param category: something like "plug", "setting", "button", "selector",
       "label", which belong to the static set self.categories
    :param value: a range value if any; defaults to 0
    :param unit: a unit like "V", "A", "ohm"; defaults to ""
    :param acdc: alternative or direct current ("ac", "dc"); defaults to "dc"
    :param parent: a link to a parent, defaults to None
    """

    categories = set(("plug", "setting", "button", "selector",
                      "label", "display"))
    units = set(("V", "A", "ohm"))
    currents = set(("ac", "dc"))

    def __init__(self,
                 name,
                 comment,
                 category,
                 value=0.0,
                 unit="",
                 acdc="dc",
                 parent=None):
        self.name = name
        self.comment = comment
        self.category = category
        self.value = value
        self.unit = unit
        self.acdc = acdc
        self.parent=parent
        assert(self.category in self.categories)
        assert(0+self.value == self.value)
        assert(not self.unit or self.unit in self.units)
        assert(self.acdc in self.currents)
        return

    def __str__(self):
        result = f"""Element [{self.category}: « {self.name} », {self.comment}"""
        if self.unit and self.unit != "ohm":
            result += f""", {self.value} {self.unit} {self.acdc}"""
        if self.unit and self.unit == "ohm":
            result += f""", {self.value} Ω"""
        result+="]"
        return result

class Multimeter:
    """
    describes a multimeter which can appear in the GUI

    Constructor's parameters:

    :param svg: the path to a SVG image; defaults to "img/multimetre.svg"
    """

    def __init__(self, svg = "img/multimetre.svg"):
        self.doc = minidom.parse(svg)
        self.todo = []
        self.children = []
        from svg_keywords import getKeywords
        for kw in getKeywords(self.doc):
            if not kw in self.__dict__:
                el=self.find_element(kw)
                if el and \
                   el.hasAttribute("data-comment") and \
                   el.hasAttribute("data-category"):
                    value = el.getAttribute("data-value")
                    if value:
                        value=float(value)
                    else:
                        value=0.0
                    acdc = el.getAttribute("data-acdc") or "dc"
                    self.children.append(multimeterElement(
                        kw,
                        el.getAttribute("data-comment"),
                        el.getAttribute("data-category"),
                        value = value,
                        unit = el.getAttribute("data-unit"),
                        acdc=acdc,
                        parent=self
                    ))
                else:
                    self.todo.append(kw)
        return
    
    def __str__(self):
        result = f"""\
Multimeter
==========
ELEMENTS BY CATEGORY:

{self.elementsByCategory}"""
        if self.todo:
            result+="""
UNKNOWN ELEMENTS = {self.todo}"""
        return result
    
    @property
    def elementsByCategory(self):
        cats=sorted(list(multimeterElement.categories))
        result=""
        for c in cats:
            result += c + "\n" + "-" * len(c) + "\n"
            elements=[f"  {a}\n" for a in self.children if a.category == c]
            result += "".join(elements)
        return result

    def find_element(self, kw, root=None):
        """
        finds an element with attribute "id" == kw
        
        :param kw: a keyword
        :param root: the root of the tree to search; if root == None
           (the default), the search will begin with self.doc
        :return: the first element found with attribute "id" == kw, or None
        """
        if root == None:
            root = self.doc
        found=None
        if root.attributes and root.getAttribute("id") == kw:
            found = root
        else:
            for el in root.childNodes:
                found = self.find_element(kw, el)
                if found:
                    break
        return found
        

    def missing(self, substr=""):
        """
        Used to report missing keywords, i.e. keywords found in the
        SVG file which describes the multimeter, but which are not
        multimeterElement attributes of self

        :param substr: a substring to filter missing elements; defaults
          to an empty string, which filters all
        """
        return [el for el in self.todo if substr in el]

if __name__ == "__main__":
    m=Multimeter()
    print(m)

    
